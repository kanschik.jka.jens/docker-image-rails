# Base image for Rails

This base image contains:
* ruby in version 1.8.7-p370
* rubygems in version v1.8.30
* rake in version 10.4.2
* rails in version 2.2.2


